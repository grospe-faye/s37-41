const Product = require('../models/Product');
const auth = require('../auth');

// Create Product (Admin Only)
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false
		} else{
			return true
		}
	})
}

// Retrieve all products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result
	})
}

// Retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}

// Retrieve single/specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}

// Update a product (Admin Only)
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false
		} else{
			return true
		}
	})
}

// Archive a product (Admin Only)
module.exports.archiveProduct = (reqParams, reqBody) => {
	let archiveProduct = {
		isActive: reqBody.isActive
	}
	return Product.findByIdAndUpdate(reqParams.productId, archiveProduct).then((product, error) =>{
		if(error){
			return false
		} else{
			return true
		}
	})
}
